# xkcd-wallpaper

Script uses a few shell tools, `wget`, `curl`, `basename` and `feh` to set last
[xkcd](https://xkcd.com/) comic as a wallpaper on Linux, centered, with white
background:

```bash
./xkcd-wall.sh
```

