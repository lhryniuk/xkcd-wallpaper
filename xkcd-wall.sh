#!/bin/bash

set -o errexit
set -o pipefail

readonly LAST_COMIC_API_URL="https://xkcd.com/info.0.json"

fail() {
    local msg="$1"
    echo -e ${msg}
    exit 1
}

is_empty() {
    local var="$1"
    [[ -z "${var}" ]] 
}

main() {
    local IMGADDR=$(curl ${LAST_COMIC_API_URL} | grep -oE '"img": "[^"]+"' | grep -oE 'http([^"]*)')
    is_empty ${IMGADDR} \
        && fail "Unable to read image address"

    local IMGNAME=$(basename ${IMGADDR})
    is_empty ${IMGNAME} \
        && fail "Unable to read image name"

    wget -O /tmp/${IMGNAME} ${IMGADDR}

    feh --bg-center -B white /tmp/${IMGNAME}
}

main
